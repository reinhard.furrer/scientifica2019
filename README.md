# scientifica2019

a push induces an update and the changes are visible at 

http://shiny.math.uzh.ch/git/reinhard.furrer/scientifica2019/ 
 
important: all shiny material has to be in the subdirectory /shiny/, possibly with further subdirs.


Workschedule:

https://drive.google.com/file/d/1jf8R1Inl89ls-MCtJruxSY0e9AADHwz0/view

The script `createZip` collects all necessary shiny app files to `shiny.zip`.
