

  # links to switch main panels
  observeEvent(input$link_to_tabpanel_spvm, {
    updateTabsetPanel(session, "panels", "spvm")
  })
  observeEvent(input$link_to_tabpanel_vtp1, {
    updateTabsetPanel(session, "panels", "vtp")
  })
  observeEvent(input$link_to_tabpanel_vtp2, {
    updateTabsetPanel(session, "panels", "vtp")
  })
  observeEvent(input$link_to_tabpanel_vtp3, {
    updateTabsetPanel(session, "panels", "vtp")
  })
  observeEvent(input$link_to_tabpanel_stats, {
    updateTabsetPanel(session, "panels", "stats")
  })
  # toggle additional info
  observe({
    toggle("delta_info", condition = input$delta_button)
  })
  observe({
    toggle("alpha_info", condition = input$alpha_button)
  })
  observe({
    toggle("ts_info", condition = input$ts_button)
  })
  observe({
    toggle("stat_info", condition = input$stat_button)
  })
  observe({
    toggle("stat_info_br", condition = input$stat_button)
  })
  # reactive parts
  r <- reactive({
    mu_1 <- input$mu_1
    n <- input$n
    a <- input$a
    twosided <- input$twosided
    list(mu_1 = mu_1, n = n, a = a, twosided = twosided)
  })
  # reactive outputs
  output$formula <- renderUI({
    r <- r()
    withMathJax(
      paste0("\\(\\delta = \\frac{", r$mu_1, "-40}{4} = ",
        format(round((r$mu_1 - 40) / 4, 2), nsmall = 2), "\\)).")
    )
  })
  output$mu_11 <- renderUI({
    r <- r()
    withMathJax(
      paste0("\\(\\mu = ", r$mu_1, "\\))")
    )
  })
  output$mu_12 <- renderUI({
    r <- r()
    withMathJax(
      paste0("\\(\\mu = ", r$mu_1, "\\)")
    )
  })
  output$mu_13 <- renderUI({
    r <- r()
    withMathJax(
      paste0("\\(\\mu = ", r$mu_1, "\\)")
    )
  })
  output$mu_14 <- renderUI({
    r <- r()
    withMathJax(
      paste0("\\(", r$mu_1, "\\)")
    )
  })
  output$mu_15 <- renderUI({
    r <- r()
    withMathJax(
      paste0("\\(", r$mu_1, "\\)")
    )
  })
  output$n1 <- renderUI({
    r <- r()
    withMathJax(
      paste0("\\(n = ", r$n, "\\)")
    )
  })
  output$n2 <- renderUI({
    r <- r()
    withMathJax(
      paste0("\\(n = ", r$n, "\\)")
    )
  })
  output$stat <- renderUI({
    r <- r()
    withMathJax(
      paste0("\\(",
        format(round(sqrt(r$n) * ((r$mu_1 - 40) / 4), 2), nsmall = 2), "\\).")
    )
  })
  # main panel spvm
  output$distplot <- renderPlot({
    r <- r()
    par(mai = c(0.5, 0.7, 0.1, 0.1), mgp = c(2, 0.7, 0))
    curve(dnorm(x, 40, 4 / sqrt(r$n)), from = 36, to = 49, xlab = "", ylab = "",
      col = "black", main = "", yaxs = "i", bty = "n")
    curve(dnorm(x, r$mu_1, 4 / sqrt(r$n)), add = TRUE, col = "#428BCA")
    expr1 <- bquote(N(mu[bar(x)], sigma[bar(x)])~"unter"~H[0]:~
      N(40, frac(4, sqrt(.(r$n)))))
    expr2 <- bquote(N(mu[bar(x)], sigma[bar(x)])~"unter"~H[1]:~
      N(.(r$mu_1), frac(4, sqrt(.(r$n)))))
    legend("topleft", legend = do.call("expression", list(expr1, expr2)),
      lty = c(1, 1), col = c("black", "#428BCA"), bty = "n", y.intersp = 2)
    box(bty = "L", lwd = 2)
  })
  # main panel vtp
  output$alphaPower <- renderPlot({
    r <- r()
    lg <- rgb(169, 169, 169, 50, maxColorValue = 255) # grey color
    min <- -5
    max <- 11
    stat <- sqrt(r$n) * ((r$mu_1 - 40) / 4)
    if(r$twosided) {
      quant <- 1 - as.numeric(r$a) / 2
      pw <- round(1 - (pnorm((((40 + qnorm(1 - as.numeric(r$a) / 2) * 4 /
        sqrt(r$n)) - r$mu_1) / (4 / sqrt(r$n)))) -
        pnorm((((40 - qnorm(1 - as.numeric(r$a) / 2) * 4 /
        sqrt(r$n)) - r$mu_1) / (4 / sqrt(r$n))))), 3)
    } else {
      quant <- 1 - as.numeric(r$a)
      pw <- round(1 - pnorm(qnorm(1 - as.numeric(r$a)) -
        (sqrt(r$n) * (r$mu_1 - 40) / 4)), 3)
    }
    if(pw  == 1.000) pw <- 0.999
    pw <- format(pw, nsmall = 3)
    s <- s_1 <- s_u <- seq(min, max, length = 10000)
    d <- dnorm(s)
    q <- qnorm(quant)
    sq <- c(s[s > q], max, q)
    dq <- c(d[s > q], 0, 0)
    quant_1 <- round(pnorm(qnorm(1 - as.numeric(r$a)) -
      (sqrt(r$n) * (r$mu_1 - 40) / 4)), 2)
    d_1 <- dnorm(s_1, stat)
    q_1 <- qnorm(quant)
    sq_1 <- c(s_1[s_1 < q_1], q_1, min)
    dq_1 <- c(d_1[s_1 < q_1], 0, 0)
    sq_p <- c(s_1[s_1 > q_1], max, q_1)
    dq_p <- c(d_1[s_1 > q_1], 0, 0)
    d_u <- dnorm(s_u)
    q_u <- qnorm(1 - quant)
    sq_u <- c(s_u[s_u < q_u], q_u, min)
    dq_u <- c(d_u[s_u < q_u], 0, 0)
    par(mai = c(0.7, 0.7, 0.1, 0.1), mgp = c(2, 1, 0), xpd = TRUE)
    plot(s, d, type = "l", xlab = "", ylab = "", ylim = c(0, max(d)),
      yaxs = "i", axes = FALSE)
    axis(side = 1, at = 0, tck = -0.01, labels = 0)
    if(r$mu_1 != 40) {
      axis(side = 1, at = stat, tck = -0.01,
        labels = format(round(stat, 2), nsmall = 2))
    }
    axis(side = 2)
    segments(x0 = q_1, y0 = 0, x1 = q_1, y1 = dnorm(qnorm(quant)))
    polygon(x = sq, y = dq, col = "darkred")
    xs <- c(q_1, 4)
    arrows(x0 = xs[2], y0 = -0.01, x1 = xs[1], y1 = -0.01, length = 0.05,
      code = 2, angle = 90, col = "darkred")
    if(r$twosided) {
      segments(x0 = -q_1, y0 = 0, x1 = -q_1, y1 = dnorm(qnorm(quant)))
      polygon(x = sq_u, y = dq_u, col = "darkred")
      arrows(x0 = -xs[2], y0 = -0.01, x1 = -xs[1], y1 = -0.01, length = 0.05,
        code = 2, angle = 90, col = "darkred")
    }
    lines(s_1, d_1, type = "l", col = "#428BCA")
    polygon(x = sq_1, y = dq_1, col = lg, border = "#428BCA")
    segments(x0 = qnorm(quant), y0 = 0, x1 = qnorm(quant),
      y1 = dnorm(qnorm(quant), stat))
    polygon(x = sq_p, y = dq_p, col = "darkgreen", density = 10, border = NA)
    if(r$twosided) {
      sq_pu <- c(s_1[s_1 < - q_1], -q_1, min)
      dq_pu <- c(d_1[s_1 < -q_1], 0, 0)
      polygon(x = sq_pu, y = dq_pu, col = "darkgreen", density = 10,
        border = NA)
    }
    lgd <- legend("topleft", c("Ablehnbereich", expression(alpha),
      expression(beta), expression(1 - beta)),
      border = c("white", "black", "black", "black"),
      fill =  c("white", "darkred", lg, "darkgreen"),
      density = c(NA, NA, NA, 20), bty = "n")
    arrows(x0 = lgd$text$x[1] - 0.19, y0 = lgd$text$y[1],
      x1 = lgd$text$x[1] - 0.32, y1 = lgd$text$y[1], length = 0.05, code = 2,
      angle = 90, col = "darkred")
    text(x = stat + 1.9, y = max(d) - 0.01,
      labels = bquote("Power"~(1 - beta)~"="~.(pw)), cex = 1.3)
    box(bty = "L", lwd = 2)
  })


output$renderSchuleUI <- renderUI({
  if (input$num == 0)                                                { return(NULL) }

  list(
    tags$head(tags$style(HTML("hr {border-top: 1px solid #000000;};"))),
    titlePanel("Der Ein-Stichproben z-Test:"), withMathJax(),
        sidebarLayout(
            sidebarPanel(width = 5,
            HTML("An einer Primarschule wurde bisher vorwiegend
                  Frontalunterricht abgehalten. Mit dieser Unterrichtsform lag
                  die Leistung der Schülerinnen und Schüler bei einem
                  Abschlusstest im Mittel bei \\(\\mu_0 = 40\\) Punkten.
                  Die Werte waren um diesen Mittelwert normalverteilt mit einer
                  Standardabweichung von \\(\\sigma = 4\\) Punkten.<br>
                  Nun wird eine neue, interaktivere Unterrichtsform eingeführt.
                  Für diese neue Unterrichtsform erwarten wir im Durchschnitt
                  bessere Leistungen beim Abschlusstest. (Von der
                  Standardabweichung nehmen wir hingegen an, dass sie sich
                  durch die neue Unterrichtsform nicht ändert.)<br>
                  Der Mittelwert der Leistungen für die neue Unterrichtsform
                  &mu; kann mit dem Schieber unten in ganzzahligen Schritten
                  von \\(40\\) (d.h. keine Verbesserung der Leistung) bis
                  \\(45\\) (d.h. starke Verbesserung der Leistung) variiert
                  werden. Zusätzlich kann der Stichprobenumfang \\(n\\) variiert
                  werden. Die Grafiken auf der rechten Seite ändern sich
                  entsprechend dieser Eingaben."),
            sliderInput(inputId = "mu_1",
                        label = HTML("\\(\\mu\\)"),
                        min = 40,
                        max = 45,
                        value = 43,
                        step = 1,
                        ticks = FALSE),
            sliderInput(inputId = "n",
                        label = HTML("\\(n\\)"),
                        min = 12,
                        max = 30,
                        value = 12,
                        step = 1,
                        ticks = FALSE,
                        animate = TRUE),
            HTML("Der Unterschied zwischen der mittleren Leistung unter der
                  alten Unterrichtsform und der erwarteten mittleren Leistung
                  unter der neuen Unterrichtsform kann auch durch die
                  standardisierte Effektstärke \\(\\delta\\) ausgedrückt werden:
                  \\(\\delta = \\frac{\\mu-\\mu_0}{\\sigma}\\)
                  (hier ergibt sich mit dem oben eingestellten Wert"),
            uiOutput("formula", inline = TRUE),
            checkboxInput("delta_button",
                          HTML("Zusatzinfo zu \\(\\delta\\) einblenden")),
            div(id = "delta_info", style = "background: #b3d0e9;",
                HTML("Die standardisierte Effektstärke beschreibt, um wie viele
                      <u>Standardabweichungen</u> sich die mittlere Leistung
                      durch die neue Unterrichtsform verändert (wohingegen der
                      unstandardisierte Unterschied zwischen \\(\\mu\\) und
                      \\(\\mu_0\\) aussagt, um wie viele <u>Punkte</u> im
                      Abschlusstest sie sich verändert). Für die standardisierte
                      Effektstärke gibt es nach Cohen grobe Richtlinien zur
                      Interpretation; z.B. wird bei Mittelwertsvergleichen eine
                      standardisierte Effektstärke ab 0.2 als kleiner, ab 0.5
                      als mittelgroßer und ab 0.8 als großer Effekt
                      bezeichnet.")),
             hr(),
             HTML("Nun führen wir einen statistischen Test durch, um zu
                   überprüfen, ob die neue Unterrichtsform zu signifikant
                   besseren Leistungen führt als die alte. (Da die Werte
                   normalverteilt sind, und wir hier der Einfachkeit halber so
                   tun, als ob wir die wahre Standardabweichung kennen würden,
                   können wir hierfür den Ein-Stichproben z-Test verwenden.)<br>
                   <br>
                   Die Nullhypothese lautet:
                   \\(H_0 \\colon ~ \\mu = \\mu_0\\),
                   d.h. die Leistung bleibt im Mittel gleich.<br>
                   <br>
                   Die Alternativhypothese lautet:
                   \\(H_1 \\colon ~ \\mu > \\mu_0\\),
                   d.h. die Leistung verbessert sich im Mittel.<br>
                   <br>
                   Im Feld"),
             actionLink("link_to_tabpanel_spvm",
                        "Stichprobenverteilung des Mittelwerts"),
             HTML("auf der
                   rechten Seite sieht man, wie sich die Stichprobenverteilung
                   des Mittelwerts unter \\(H_0\\) (ohne Veränderung der
                   Leistung, d.h. mit Mittelwert \\(\\mu = \\mu_0 = 40\\))
                   und unter \\(H_1\\) (mit Veränderung der Leistung auf
                   den von Ihnen eingestellten Wert"),
             uiOutput("mu_11", inline = TRUE),
             HTML("entsprechend Ihren Einstellungen verändert. Im zweiten
                   Feld"),
             actionLink("link_to_tabpanel_vtp1",
                        "Verteilung der Prüfgröße"),
             HTML("ist die Testentscheidung dargestellt."),
             hr(),
             HTML("Um den Test durchführen zu können, und auch um die Power des
                   Tests für die oben festgelegt Effektstärke berechnen zu
                   können, müssen wir das \\(\\alpha\\)-Niveau des Tests
                   festlegen:"),
             checkboxInput("alpha_button",
                           HTML("Zusatzinfo zu \\(\\alpha\\) einblenden")),
             div(id = "alpha_info", style = "background: #b3d0e9;",
                 HTML("Mit \\(\\alpha\\) wird die Obergrenze für den Fehler 1.
                       Art festgelegt (d.h. für die Wahrscheinlichkeit, die
                       Nullhypothese abzulehnen, obwohl sie zutrifft und in
                       Wahrheit gar kein Effekt existiert).")),
             selectInput(inputId = "a",
                         label = "",
                         choices = list("\u03B1 = 0.01" = 0.01,
                                        "\u03B1 = 0.05" = 0.05,
                                        "\u03B1 = 0.1" = 0.1),
                         selected = 0.05,
                         width = "20%",
                         selectize = TRUE),
             hr(),
             HTML("Möchten Sie einen einseitigen oder einen zweiseitigen Test
                   durchführen?"),
             checkboxInput("twosided",
                           "Wechsel zum zweiseitigen Test"),
             checkboxInput("ts_button",
                           HTML("Zusatzinfo zum zweiseitigen Test einblenden")),
             div(id = "ts_info", style = "background: #b3d0e9;",
                 HTML("Der einseitige Test entspricht der oben beschriebenen
                       gerichteten Alternativhypothese, dass sich durch die
                       neue Unterrichtsform die Leistung im Mittel
                       verbessert.<br>
                       Der zweiseitige Test entspricht hingegen der
                       ungerichteten Alternativhypothese, dass sich durch die
                       neue Unterrichtsform die Leistung im Mittel verändert
                       (man legt sich aber nicht fest, ob man eine Verbesserung
                       oder Verschlechterung erwartet).<br>
                       Wie Sie rechts im Feld"),
                  actionLink("link_to_tabpanel_vtp2",
                             "Verteilung der Prüfgröße"),
                  HTML("sehen können, hat der einseitige Test &mdash; sofern man
                        mit der erwarteten Richtung des Effekts richtig liegt
                        &mdash; eine höhere Power als der zweiseitige Test.")),
             hr(),
             HTML("Für die Testentscheidung wird aus den Daten eine Prüfgröße
                   berechnet. Fällt diese in den durch Ihre Wahl von
                   \\(\\alpha\\) festgelegten Ablehnbereich, kann die
                   Nullhypothese abgelehnt werden. In diesem Fall würde die neue
                   Unterrichtsform zu einer signifikanten Verbesserung der
                   Leistung im Abschlusstest führen."),
             checkboxInput("stat_button",
                           HTML("Zusatzinfo zur Prüfgröße einblenden")),
             div(id = "stat_info", style = "background: #b3d0e9;",
                 HTML("Die Formel zur Berechnung der Prüfgröße können Sie
                       rechts im Feld"),
                 actionLink("link_to_tabpanel_stats",
                            "Statistischer Hintergrund"),
                 HTML("finden. Insbesondere geht darin der Mittelwert aus den
                       Daten, \\(\\bar{x}\\), ein. Ist dieser weit von
                       \\(\\mu_0\\) entfernt, spricht dies für eine
                       Veränderung der mittleren Leistung, d.h. für die
                       Ablehnung der
                       Nullhypothese.<br>")),
             div(id = "stat_info_br", HTML("<br>")),
             HTML("Im zweiten Feld"),
             actionLink("link_to_tabpanel_vtp3",
                        "Verteilung der Prüfgröße"),
             HTML("auf der rechten Seite sieht man, wie sich die Verteilung der
                   Prüfgröße unter \\(H_0\\) und unter \\(H_1\\)
                   entsprechend Ihren Einstellungen verändert.<br>
                   Neben dem von Ihnen festgelegten \\(\\alpha\\)-Niveau für den
                   Fehler 1. Art kann man an dieser Darstellung auch ablesen,
                   welcher Fehler 2. Art \\(\\beta\\) und welche Power
                   \\(1-\\beta\\) sich für die oben eingestellten Werte für
                   Effektgröße und Stichprobengröße ergeben.")
            ),
            mainPanel(width = 7,
                tabsetPanel(id = "panels",
                tabPanel(value = "spvm",
                         HTML("Stichprobenverteilung des Mittelwertes"),
                         plotOutput("distplot"),
                         HTML("Diese Grafik zeigt, was passieren würden, wenn
                               man unendlich viele Stichproben mit jeweils"),
                         uiOutput("n1", inline = TRUE),
                         HTML("Personen ziehen würde, aus diesen Stichproben
                               jeweils den Mittelwert berechnet, und die
                               Verteilung dieser Mittelwerte darstellt.<br>
                               <br>"),
                         HTML("Links sehen Sie die Verteilung unter der
                               Nullhypothese. Wenn der wahre Mittelwert
                               weiterhin bei \\(\\mu_0 = 40\\) liegt, und
                               wir unendlich viele Stichproben ziehen, werden
                               deren berechnete Mittelwerte zufällig um
                               \\(\\mu_0 = 40\\) schwanken, wobei Werte in
                               der Nähe von \\(40\\) häufiger vorkommen als
                               Werte, die weit davon entfernt liegen.<br>
                               <br>"),
                         HTML("Wenn Sie den Stichprobenumfang \\(n\\) auf der
                               linken Seite erhöhen, sehen Sie, dass sich die
                               Stichprobenverteilung des Mittelwerts noch enger
                               um den wahren Mittelwert zusammenzieht, weil
                               berechnete Mittelwerte aus größeren Stichproben
                               näher am wahren Wert landen.<br>
                               <br>"),
                         HTML("Rechts sehen Sie die Verteilung unter der
                               Alternativhypothese. Wenn der wahre Mittelwert
                               bei dem von Ihnen eingestellten Wert von"),
                         uiOutput("mu_12", inline = TRUE),
                         HTML("liegt, und wir unendlich viele Stichproben
                               ziehen, werden deren berechnete Mittelwerte
                               zufällig"),
                         uiOutput("mu_13", inline = TRUE),
                         HTML("schwanken, wobei Werte in der Nähe von"),
                         htmlOutput("mu_14", inline = TRUE),
                         HTML("häufiger vorkommen als Werte, die weit davon
                               entfernt liegen.<br>
                               <br>"),
                         HTML("Auch diese Verteilung zieht sich zusammen,
                               wenn die Stichprobengröße erhöht wird. Wenn Sie
                               hingegen für \\(\\mu\\) einen anderen Wert
                               einstellen, verschiebt sich der Mittelwert und
                               damit die Lage der gesamten Verteilung nach
                               links oder rechts.")),
                tabPanel(value = "vtp",
                         HTML("Verteilung der Prüfgröße"),
                         plotOutput("alphaPower"),
                         HTML("Diese Grafik zeigt, was passieren würden, wenn
                               man unendlich viele Stichproben mit jeweils"),
                         uiOutput("n2", inline = TRUE),
                         HTML("Personen ziehen würde, aus diesen Stichproben
                               jeweils den Wert der Prüfgröße (auch genannt
                               Teststatistik) berechnet, und die Verteilung
                               dieser Werte darstellt.<br>
                               <br>"),
                         HTML("Links sehen Sie die Verteilung unter der
                               Nullhypothese. Wenn der wahre Mittelwert
                               weiterhin bei \\(\\mu_0 = 40\\) liegt,
                               ergibt sich für die Prüfgröße im Mittel der
                               Wert \\(0\\).<br>
                               <br>"),
                         HTML("Rechts sehen Sie die Verteilung unter der
                               Alternativhypothese. Wenn der wahre Mittelwert
                               bei dem von Ihnen eingestellten Wert von"),
                         uiOutput("mu_15", inline = TRUE),
                         HTML("liegt, ergibt sich für die Prüfgröße im Mittel
                              der Wert"),
                         uiOutput("stat", inline = TRUE),
                         HTML("<br>
                               <br>
                               Wenn Sie den Wert von \\(\\mu\\) auf der linken
                               Seite erhöhen, sehen Sie, dass die Verteilungen
                               der Prüfgröße unter \\(H_0\\) und \\(H_1\\)
                               weiter auseinanderliegen. D.h. größere Effekte
                               führen zu einer klareren Testentscheidung und
                               einer höheren Power.<br>
                               <br>"),
                         HTML("Auch wenn Sie den Stichprobenumfang \\(n\\) auf
                               der linken Seite erhöhen, sehen Sie, dass die
                               Verteilungen der Prüfgröße unter \\(H_0\\)
                               und \\(H_1\\) weiter auseinanderliegen.
                               D.h. größere Stichproben führen zu einer
                               klareren Testentscheidung und einer höheren
                               Power.<br>
                               <br>"),
                         HTML("Wenn Sie das \\(\\alpha\\)-Niveau auf der linken
                               Seite verändern, sehen Sie, dass ein kleineres
                               \\(\\alpha\\)-Niveau zu einer niedrigeren und ein
                               größeres \\(\\alpha\\)-Niveau zu einer höheren
                               Power führen. (Falls Sie keine Veränderung
                               erkennen, wählen Sie einen weniger extremen Wert
                               für \\(\\mu\\).)<br>
                               <br>
                               Ein \\(\\alpha\\)-Niveau von \\(1\\%\\) bedeutet
                               z.B., dass nur in \\(1\\%\\) der Fälle die
                               Nullhypothese fälschlicherweise abgelehnt werden
                               darf, obwohl sie zutrifft. Um dieses strenge
                               \\(\\alpha\\)-Niveau einhalten zu können, wird der
                               in Rot dargestellte Ablehnbereich verkleinert.
                               Dadurch führt der Test insgesamt zu weniger
                               Ablehnungen der Nullhypothese &mdash; auch in
                               Fällen, in denen es richtig wäre, sie
                               abzulehnen."),
                         HTML("Ähnlich sehen Sie, dass wenn sie auf der linken
                               Seite zum zweiseitigen Test wechseln, das
                               gewählte \\(\\alpha\\)-Niveau auf die zwei Seiten
                               aufgeteilt wird. Dadurch sind das verbleibende
                               \\(\\alpha\\) sowie der Ablehnbereich in Richtung
                               der Alternativhypothese kleiner und die Power
                               sinkt.")),
                tabPanel(value = "stats",
                         HTML("Statistischer Hintergrund"), withMathJax(),
                         HTML("<b>Stichprobenverteilung des Mittelwerts:</b>
                               <br>"),
                         HTML("Wenn die einzelnen Werte \\(x_i\\) normalverteilt
                               sind mit Mittelwert \\(\\mu\\) und
                               Standardabweichung \\(\\sigma\\), dann sind die
                               Mittelwerte aus Stichproben der Größe \\(n\\)
                               normalverteilt mit Mittelwert \\(\\mu\\) und
                               Standardabweichung
                               \\(\\frac{\\sigma}{\\sqrt{n}}\\).<br>"),
                         HTML("<br><b>Verteilung der Prüfgröße:</b><br>"),
                         HTML("Die Prüfgröße \\(\\textrm{z} = \\sqrt{n}
                               \\Big(\\frac{\\bar{x} -
                               \\mu_{0}}{\\sigma}\\Big)\\) ist unter der
                               Nullhypothese standardnormalverteilt. Als
                               Ablehnbereich verwendet man im einseitigen Fall
                               \\(\\textrm{z} > \\textrm{z}_{1 - \\alpha}\\)
                               und im zweiseitigen Fall \\(|\\textrm{z}| >
                               \\textrm{z}_{1 - \\frac{\\alpha}{2}}\\). <br>"),
                         HTML("Unter der Alternativhypothese ist die Prüfgröße
                               normalverteilt mit Mittelwert \\(\\sqrt{n}
                               \\Big(\\frac{\\mu - \\mu_{0}}{\\sigma}\\Big)\\)
                               und Standardabweichung 1.<br>"),
                         HTML("Diese Formeln ergeben sich nach allgemeinen
                               statistischen Prinzipien aus den Annahmen des
                               z-Tests und wurden in den Grafiken für diese App
                               verwendet."))
                )
            )
        )
)})
