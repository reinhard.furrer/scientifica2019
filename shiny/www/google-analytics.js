(function(i,s,o,g,r,a,m){
  i['GoogleAnalyticsObject']=r;
  i[r]=i[r]||
  function(){
    (i[r].q=i[r].q||[]).push(arguments);
  },i[r].l=1*new Date();
  a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];
  a.async=1;
  a.src=g;
  m.parentNode.insertBefore(a,m);
})(window,document,'script',
  'https://www.google-analytics.com/analytics.js','ga');


ga('create', 'UA-106093676-1', 'auto');
ga('send', 'pageview');


$(document).on('shiny:inputchanged', function(event) {
  if(event.name == 'btn1' || event.name == 'btn2' ||
     event.name == 'btn3' || event.name == 'btn4' ||
     event.name == 'btn5' || event.name == 'btn6') {
       ga('send', 'event', 'input', 'updates', event.name, event.value);
     }
});
